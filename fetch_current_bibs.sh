#!/bin/bash

git clone ssh://git@gitlab.cern.ch:7999/atlas-physics-office/atlaslatex.git TempAtlasLatex

mv TempAtlasLatex/bib .
rm -rf TempAtlasLatex
